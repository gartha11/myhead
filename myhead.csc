class blah {

   public static void Main(string[] args) {
         // string s;
          System.Text.StringBuilder sb = new System.Text.StringBuilder();
 
           if(args.Length>2)   { System.Console.WriteLine("Error: You have passed too many parameters. Run the program with no parameters and see the usage"); System.Environment.Exit(1); }

          if(args.Length==0) {
               bool error=false;
               PrintUsage(0);
              //sb.AppendLine();

              bool anythingpiped=false; // anything can be assigned
              try {  if(System.Console.KeyAvailable) anythingpiped=false;   } 
              catch(System.InvalidOperationException) { anythingpiped=true; }
              
               if(anythingpiped)  {  error=true; System.Console.WriteLine("Error: You have piped data to the program, you should not have, you should only pipe data in when passing one parameter, a numeric parameter indicating how many lines to read from the file, and you pipe the contents of the file e.g. echo abc| myhead 1   or  type abc.txt | myhead 3"); } 
             if(error) System.Environment.Exit(1); else System.Environment.Exit(0);
           }

           if(args.Length==2) {
                double n;
                int intn=-999;
                bool error=false;
                bool isNumeric = double.TryParse(args[0], out n);                 
                bool isInteger=false; // can be assigned any value

               if(!isNumeric) {
                      System.Console.WriteLine("Error: The first parameter you gave was not a number. It should be a number - specifically, an integer of one or more, as it's how many lines you want to display from the file starting from the top/beginning");  
                      error=true;
                }
                else  {               
                     isInteger = int.TryParse(args[0], out intn);
                     if(!isInteger) {
                          System.Console.WriteLine("Error: The first parameter you gave was not an integer. It must be an integer");
                          error=true;
                      }
                 }
             
               

                 //validation of second parameter, still within if(args.Length==2)
 
                  if(!System.IO.File.Exists(args[1])) {
                           System.Console.WriteLine("Error: File "+args[1] + " specified as second/last parameter, does not exist, so if you gave an absolute path then it doesn't exist there and if you gave a relative path then it doesn't exist in current directory " + System.IO.Directory.GetCurrentDirectory());
                           error=true;
                  }        


                 if(isNumeric && isInteger && intn<1) {
                   System.Console.WriteLine("Error: The parameter you gave for number of lines to read from the top/beginning of the file, that's the first of the two parameters, should have a value of one or more. Yours is it not yours is " + intn);                 
                   error=true;
                 }

                  if(error) System.Environment.Exit(1);
                 
                 //some  from http://stackoverflow.com/questions/7980456/reading-from-a-text-file-in-c-sharp
                 System.Collections.Generic.List<string> lststr = new System.Collections.Generic.List<string>();
                 using (var reader = new System.IO.StreamReader(args[1]))
                 {
                      string line;
                       int i=0;
                      while ((line = reader.ReadLine()) != null)
                      {
                             i++;
                             lststr.Add(line);
                             if(i==intn) break;
                      }

                      for(i=0; i<lststr.Count; i++)  System.Console.WriteLine(lststr[i]);
                 }

           }
           if(args.Length==1) {    //[4]
                double n;
                int intn=-999;
                bool error=false;
                bool isNumeric = double.TryParse(args[0], out n);                 
                bool isInteger=false;  // can be assigned any value

                 bool anythingpiped=false; // anything can be assigned
                if(args[0].Equals("-?") || args[0].Equals("/?") || args[0].ToLower().Equals("-h") || args[0].ToLower().Equals("help") || args[0].ToLower().Equals("--help") ) {
                     
                   
                    try {  if(System.Console.KeyAvailable) anythingpiped=false;   } 
                   catch(System.InvalidOperationException) { anythingpiped=true; }
              
                   if(anythingpiped)  {  error=true; System.Console.WriteLine("Error: You have piped data to the program, and given a parameter of "+args[0] + " that males no sense.  You can  run the program with no parameters or -? to see usage but don't pipe to it when doing so as that makes no sense and is not in line with the correct usage of the program"); } 
                  if(error) System.Environment.Exit(1); else {PrintUsage(1); System.Environment.Exit(0); }
 
                   

                 }

               if(!isNumeric) {
                      System.Console.WriteLine("Error: The parameter you gave was not a number. It should be a number - specifically, an integer of one or more, as it's how many lines you want to display from the file starting from the top/beginning"); 
                      error=true;
                }
                else  {               
                     isInteger = int.TryParse(args[0], out intn);
                     if(!isInteger) {
                          System.Console.WriteLine("Error: The parameter you gave was not an integer. It must be an integer");
                          error=true;
                      }
                 }
                     
               if(isNumeric && isInteger && intn<1) {
                   System.Console.WriteLine("Error: The parameter you specified should be one or more. It's how many lines you want to display from the file starting from the top/beginning");                 
                   error=true;
               }

            
             //System.Console.WriteLine("aaa");
              string s;
              //http://stackoverflow.com/questions/199528/c-sharp-console-receive-input-with-pipe

             // http://stackoverflow.com/questions/40066405/c-sharp-how-can-i-read-data-piped-to-my-program-without-prompting-when-nothing 
             // http://stackoverflow.com/questions/199528/c-sharp-console-receive-input-with-pipe

              //anythingpiped=false; // anything can be assigned
              try {  if(System.Console.KeyAvailable) anythingpiped=false;   } 
              catch(System.InvalidOperationException) { anythingpiped=true; }

             if(!anythingpiped)  {
                 System.Console.WriteLine("Error:  When or even when you have one parameter specifying how many lines to display from a file, and no other parameter specifying a filename, you have to pipe some contents e.g. type a.txt | myhead 2 And you have not piped anything, you haven't even piped an empty line, you've piped nothing. You have to pipe something in this case, where the filename isn't specified as a parameter"); 
                  error=true; 
              }

              if(error) System.Environment.Exit(1);
              sb = new System.Text.StringBuilder();
              int i=0;
              while((s=System.Console.ReadLine())!=null)
              {
                  i++;
                 sb.AppendLine(s);
                  if(i==intn) break;                 
              }
            //  if(i==0) {  System.Console.WriteLine("Error:  When you have one parameter specifying how many lines to display from a file, and no other parameter specifying a filename, you have to pipe some contents e.g. type a.txt | myhead 2 And you have not piped anything, you haven't even piped an empty line, you've piped nothing. You have to pipe something in this case, where the filename isn't specified as a parameter"); System.Environment.Exit(1);  }

              System.Console.Write(sb.ToString());
           }     

   } //main

   public static void PrintUsage(int a) {
              // a is args.Length  seems inaccessible outside of main method so I passed it as a parameter
              // prints usage and exits with no error.. slightly different message if -? vs if no parameter.
              // supports some other things than -? 
              System.Text.StringBuilder sb = new System.Text.StringBuilder();
              sb.AppendLine("Usage:");   sb.AppendLine();
             sb.AppendLine(@"You can pipe to it");
              sb.AppendLine(@"Example: C:\blah>type file| myhead 2"); sb.AppendLine(); // [1]
              sb.AppendLine(@"You can use a filename as parameter, in which case, make it the last parameter"); // [2]
              sb.AppendLine(@"Example: C:\blah>myhead 2 filename"); sb.AppendLine();
              sb.AppendLine(@"Running the program with no parameters or any of these switches/options  -? or /? or -h or --help will show a usage screen");
             if(a==0)
                 sb.AppendLine(@"Running the program with no parameters is still normal usage for showing this screen, a usage screen, and will not cause this program to exit with no error. Note though that if you pipe data to the program and don't specify one and just one parameter when doing so, then that'd be an example of an error and the program would then of course exit with an error"); // [3]
         
              // i've said "a" usage screen rather than "the" usage screen 'cos there is that one line difference depending on whether called with no parameters or with one parameter of -?/-h//?/--help 
              System.Console.WriteLine(sb.ToString());
              
   
   }

}

/*
[1] Doing type blah| whatever, when you can pass an file as a parameter, is considered inefficient.. But, it was just an example, you can usefully(that word is useFully not saying usUally), pipe to it e.g.  grep "a" blah.txt | myhead 2    And if  one were to forget or not know that filename should be the last parameter then one might use 'type' if efficiency is negligible, as it most often is.  Extra keystrokes are minimal too.. and it's not like *nix -always- seeks to minimize keystrokes.  There's no *nix native equivalent of windows and dos's dir /ad ( in *nix) that is as short on keystrokes as that(dir /ad). 


[2] "You can use a filename as parameter, in which case, make it the last parameter"
This is often the convention for other commands that take a filename as parameter,  such as *nix- grep, sed, and windows- find, findstr. An  alternative often used is specifying a switch beforehand e.g. -f blah.a   e.g. wget uses a convention rather like that (wget www.google.com -o a.a),  or (wget -o a.a www.google.com) but I haven't written this to use that convention.   Also, I could have programmed this to see which parameter is numeric and which isn't and allow for a filename to not have to be the last..  And I could've said that if both parameters are numeric then throw an error requiring a special switch -nf (numeric filename), that'd mean  that filename is numeric and is specified last.  But for the purposes of not enforcing a habit that'd make sed and find tricky to use correctly, i've said filename must be last, that way enforcing a 'good' habit in the sense of remembering the usage of those tools, as they use that convention of filename last and no switch specifying filename.

[3] as shown by    echo %errorlevel%    0=no error     non-zero=error, as is the convention.

[4]  I could technically make it so that when only one parameter is passed, that one parameter could be either a filename or another parameter specifically, a number denoting how many lines to read. But no programs really do that.. These programs like find or grep / whatever, call ti program blah, when you pipe a file to program blah,  the contents of the file or whatever is piped to program blah, is considered to be the file contents equivalent to what would be if you'd put that filename as a parameter.  What is piped to program blah is not considered to be some other parameter.. So I will keep to that convention. What is piped is file contents equivalent to the contents of the file that one would otherwise specify in a file parameter.
*/